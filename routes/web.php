<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    $tasks = DB::table('tasks')->get(); // to get all tasks

    return view('about', compact('tasks'));
});

Route::get('/tasks', function () {
    $tasks = DB::table('tasks')->get(); // to get all tasks

    return view('tasks.index', compact('tasks'));
});
Route::get('/tasks/{task}', function ($id) {
    $task = DB::table('tasks')->find($id); // to get specific task using task id
//    dd($task);

    return view('tasks.show', compact('task'));
});
